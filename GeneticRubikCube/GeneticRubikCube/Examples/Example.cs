﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Examples
{
    public abstract class Example<T>
    {
        public abstract GeneticSimulation<T> GetSimulation();
    }
}
