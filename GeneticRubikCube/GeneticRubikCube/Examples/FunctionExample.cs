﻿using GeneticRubikCube.Operators.CrossOver;
using GeneticRubikCube.Operators.Encode;
using GeneticRubikCube.Operators.Evaluation;
using GeneticRubikCube.Operators.Generate;
using GeneticRubikCube.Operators.Mutation;
using GeneticRubikCube.Operators.Selection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Examples
{
    class FunctionExample : Example<int>
    {
        public override GeneticSimulation<int> GetSimulation()
        {
            GeneticSimulationBuilder<int> simulationBuilder = new GeneticSimulationBuilder<int>();
            Population<int> population = new Population<int>(
                size: 10,
                crossOverProbability: 0.2,
                mutationProbability: 0.09,
                chromosomes: new List<Chromosome>(),
                encodingFunction: new BinaryEncoder(10),
                randomGenerator: new Random());
            population.Initialize(new DiscreteGenerator(0, 1023, new Random()));

            GeneticSimulation<int> simulation = simulationBuilder.
            CrossOverFunction(new TwoPointCrossOver(new Random())).
            DesiredEvaluation(int.MaxValue). 
            EvaluationFunction(new FunctionEvaluator(x => 3*x*x + 9*x + 1)).
            Iterations(200).
            MutationFunction(new SimpleMutation(new Random())).
            SelectionFunction(new ThresholdSelection(new Random(), 0.2M)).
            PopulationEvaluation(Enumerable.Max).
            Population(population).
            Build();

            return simulation;
        }
    }
}
