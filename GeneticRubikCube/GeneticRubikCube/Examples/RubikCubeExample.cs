﻿using GeneticRubikCube.Operators.CrossOver;
using GeneticRubikCube.Operators.Encode;
using GeneticRubikCube.Operators.Evaluation;
using GeneticRubikCube.Operators.Generate;
using GeneticRubikCube.Operators.Mutation;
using GeneticRubikCube.Operators.Selection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Examples
{
    public class RubikCubeExample : Example<RubiksCube>
    {
        public override GeneticSimulation<RubiksCube> GetSimulation()
        {
            RubikCubeGenerator rubiksCubeGenerator = new RubikCubeGenerator(10);
            RubiksCube initialCube = rubiksCubeGenerator.Generate();
            Population<RubiksCube> population = new Population<RubiksCube>(
                size: 20,
                crossOverProbability: 0.35,
                mutationProbability: 0.05,
                chromosomes: new List<Chromosome>(),
                encodingFunction: new RubiksCubeEncoder(initialCube),
                randomGenerator: new Random());
            population.Initialize(new RubikCubeMovesGenerator(10, new Random(), true));

            GeneticSimulationBuilder<RubiksCube> simulationBuilder = new GeneticSimulationBuilder<RubiksCube>();
            GeneticSimulation<RubiksCube> simulation = simulationBuilder.
            CrossOverFunction(new TwoPointCrossOver(new Random())).
            DesiredEvaluation(1.0).
            EvaluationFunction(new RubiksCubeCompletePercentageEvaluator()).
            Iterations(2000).
            MutationFunction(new SimpleMutation(new Random())).
            SelectionFunction(new TournamentSelection(new Random(), 3)).
            Population(population).
            PopulationEvaluation(Enumerable.Max).
            Build();

            return simulation;
        }


    }
}
