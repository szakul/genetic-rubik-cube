﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube
{
    public class RubiksCubeFace
    {
        static int size = 3;
        public int[,] face { get; set; }
        public RubiksCubeFace(int color)
        {
            face = new int[size, size];
            for (int y = 0; y < size; y++)
            {
                for (int x = 0; x < size; x++)
                {
                    face[x, y] = color;
                }
            }
        }
        public int GetColor()
        {
            return face[1, 1];
        }
        public int[] GetColumn(int index)
        {
            return face.Cast<int>().Skip(index * size).Take(size).ToArray();
        }
        public void SetColumn(int[] newColumn, int index, bool reverse = false)
        {
            for (int y = 0; y < size; y++)
            {
                face[index, y] = newColumn[y];
            }
            if (reverse)
            {
                int tmp = face[index, 0];
                face[index, 0] = face[index, size - 1];
                face[index, size - 1] = tmp;
            }
        }
        public int[] GetRow(int index)
        {
            int[] row = new int[size];
            for (int x = 0; x < size; x++)
            {
                row[x] = face[x, index];
            }
            return row;
        }
        public void SetRow(int[] newRow, int index, bool reverse = false)
        {
            for (int x = 0; x < size; x++)
            {
                face[x, index] = newRow[x];
            }
            if(reverse)
            {
                int tmp = face[0, index];
                face[0, index] = face[size - 1, index];
                face[size - 1, index] = tmp;
            }
        }

        public void TurnFaceClockwise()
        {
            int[,] tmp = new int[size, size];
            for (int i = 0; i < size; ++i)
            {
                for (int j = 0; j < size; ++j)
                {
                    tmp[i, j] = face[size - j - 1, i];
                }
            }
            face = tmp;
        }
    }
}
