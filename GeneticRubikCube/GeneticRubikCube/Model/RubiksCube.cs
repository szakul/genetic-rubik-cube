﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube
{
    public static class FaceName
    {
        public const int Front = 0;
        public const int Back = 1;
        public const int Left = 2;
        public const int Right = 3;
        public const int Up = 4;
        public const int Down = 5;
    }
    public class RubiksCube
    {
        private Random randomGenerator;
        private RubiksCubeFace[] faces = new RubiksCubeFace[6];
        public RubiksCubeFace[] Faces
        {
            get
            {
                return faces;
            }
        }
        public RubiksCube()
        {
            randomGenerator = new Random();
            for (int i = 0; i < 6; i++)
            {
                faces[i] = new RubiksCubeFace(i);
            }
        }
        public void Turn(int move)
        {
            switch(move)
            {
                case 0:
                    TurnFrontFaceCounterclockwise();
                    break;
                case 1:
                    TurnFrontFaceClockwise();
                    break;
                case 2:
                    TurnBackFaceCounterclockwise();
                    break;
                case 3:
                    TurnBackFaceClockwise();
                    break;
                case 4:
                    TurnUpFaceCounterclockwise();
                    break;
                case 5:
                    TurnUpFaceClockwise();
                    break;
                case 6:
                    TurnDownFaceCounterclockwise();
                    break;
                case 7:
                    TurnDownFaceClockwise();
                    break;
                case 8:
                    TurnLeftFaceCounterclockwise();
                    break;
                case 9:
                    TurnLeftFaceClockwise();
                    break;
                case 10:
                    TurnRightFaceCounterclockwise();
                    break;
                case 11:
                    TurnRightFaceClockwise();
                    break;
            }
        }
        public void TurnFrontFaceCounterclockwise() //F' ok
        {
            faces[FaceName.Front].TurnFaceClockwise();
            int[] tmp = faces[FaceName.Up].GetRow(2);
            faces[FaceName.Up].SetRow(faces[FaceName.Right].GetColumn(0), 2);
            faces[FaceName.Right].SetColumn(faces[FaceName.Down].GetRow(0), 0, true);
            faces[FaceName.Down].SetRow(faces[FaceName.Left].GetColumn(2), 0);
            faces[FaceName.Left].SetColumn(tmp, 2, true);
        }
        public void TurnFrontFaceClockwise()    //F
        {
            TurnFrontFaceCounterclockwise();    //tmp solution
            TurnFrontFaceCounterclockwise();
            TurnFrontFaceCounterclockwise();
        }

        public void TurnBackFaceCounterclockwise() //B' ok
        {
            faces[FaceName.Back].TurnFaceClockwise();   //tmp solution
            faces[FaceName.Back].TurnFaceClockwise();
            faces[FaceName.Back].TurnFaceClockwise();
            int[] tmp = faces[FaceName.Up].GetRow(0);
            faces[FaceName.Up].SetRow(faces[FaceName.Left].GetColumn(0), 0, true);
            faces[FaceName.Left].SetColumn(faces[FaceName.Down].GetRow(2), 0);
            faces[FaceName.Down].SetRow(faces[FaceName.Right].GetColumn(2), 2, true);
            faces[FaceName.Right].SetColumn(tmp, 2);
        }
        public void TurnBackFaceClockwise() //B
        {
            TurnBackFaceCounterclockwise();
            TurnBackFaceCounterclockwise();
            TurnBackFaceCounterclockwise();
        }

        public void TurnUpFaceCounterclockwise() //U' ok
        {
            faces[FaceName.Up].TurnFaceClockwise();
            int[] tmp = faces[FaceName.Back].GetRow(0);
            faces[FaceName.Back].SetRow(faces[FaceName.Right].GetRow(0), 0, true);
            faces[FaceName.Right].SetRow(faces[FaceName.Front].GetRow(0), 0);
            faces[FaceName.Front].SetRow(faces[FaceName.Left].GetRow(0), 0);
            faces[FaceName.Left].SetRow(tmp, 0, true);
        }
        public void TurnUpFaceClockwise()   //U
        {
            TurnUpFaceCounterclockwise();
            TurnUpFaceCounterclockwise();
            TurnUpFaceCounterclockwise();
        }

        public void TurnDownFaceCounterclockwise() //D' ok
        {
            faces[FaceName.Down].TurnFaceClockwise();
            int[] tmp = faces[FaceName.Back].GetRow(2);
            faces[FaceName.Back].SetRow(faces[FaceName.Left].GetRow(2), 2, true);
            faces[FaceName.Left].SetRow(faces[FaceName.Front].GetRow(2), 2);
            faces[FaceName.Front].SetRow(faces[FaceName.Right].GetRow(2), 2);
            faces[FaceName.Right].SetRow(tmp, 2, true);
        }
        public void TurnDownFaceClockwise() //D
        {
            TurnDownFaceCounterclockwise();
            TurnDownFaceCounterclockwise();
            TurnDownFaceCounterclockwise();
        }

        public void TurnLeftFaceCounterclockwise() // L' ok
        {
            faces[FaceName.Left].TurnFaceClockwise();
            int[] tmp = faces[FaceName.Up].GetColumn(0);
            faces[FaceName.Up].SetColumn(faces[FaceName.Front].GetColumn(0), 0);
            faces[FaceName.Front].SetColumn(faces[FaceName.Down].GetColumn(0), 0);
            faces[FaceName.Down].SetColumn(faces[FaceName.Back].GetColumn(0), 0, true);
            faces[FaceName.Back].SetColumn(tmp, 0, true);
        }
        public void TurnLeftFaceClockwise() //L
        {
            TurnLeftFaceCounterclockwise();
            TurnLeftFaceCounterclockwise();
            TurnLeftFaceCounterclockwise();
        }

        public void TurnRightFaceCounterclockwise() // R' ok
        {
            faces[FaceName.Right].TurnFaceClockwise();
            int[] tmp = faces[FaceName.Up].GetColumn(2);
            faces[FaceName.Up].SetColumn(faces[FaceName.Back].GetColumn(2), 2);
            faces[FaceName.Back].SetColumn(faces[FaceName.Down].GetColumn(2), 2, true);
            faces[FaceName.Down].SetColumn(faces[FaceName.Front].GetColumn(2), 2);
            faces[FaceName.Front].SetColumn(tmp, 2);
        }
        public void TurnRightFaceClockwise()    //R
        {
            TurnRightFaceCounterclockwise();
            TurnRightFaceCounterclockwise();
            TurnRightFaceCounterclockwise();
        }
        public void Shuffle(int moves)
        {
            for (int i = 0; i < moves; i++)
            {
                Turn(randomGenerator.Next(0, 12));
            }
        }
        public RubiksCube DeepCopy()
        {
            RubiksCube deepCopy = new RubiksCube();
            for (int i = 0; i < 6; i++)
            {
                deepCopy.faces[i].face = (int[,])faces[i].face.Clone();
            }
            return deepCopy;
        }

        public override string ToString()
        {
            StringBuilder cubeAsString = new StringBuilder();
            for (int i = 0; i < 3; i++)
                cubeAsString.Append("      " + string.Join(",", faces[FaceName.Up].GetRow(i)) + "    \n");
            for (int i = 0; i < 3; i++)
                cubeAsString.Append(string.Join(",", faces[FaceName.Left].GetRow(i)) + " " + string.Join(",", faces[FaceName.Front].GetRow(i)) + " " + string.Join(",", faces[FaceName.Right].GetRow(i)) + "\n");
            for (int i = 0; i < 3; i++)
                cubeAsString.Append("      " + string.Join(",", faces[FaceName.Down].GetRow(i)) + "    \n");
            for (int i = 0; i < 3; i++)
                cubeAsString.Append("      " + string.Join(",", faces[FaceName.Back].GetRow(i)) + "    \n");
            return cubeAsString.ToString();
        }

    }
}
