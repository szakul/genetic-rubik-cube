﻿using GeneticRubikCube.Operators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneticRubikCube.Operators.Selection;
using GeneticRubikCube.Operators.Mutation;
using GeneticRubikCube.Operators.CrossOver;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.IO;

namespace GeneticRubikCube
{
    public class Logger
    {
        //--start-- this fields help writing to file
        FileStream filestream;
        StreamWriter streamwriter;
        TextWriter oldOut = Console.Out;
        bool consoleOutWasChanged = false;
        //---end---

        protected IList<double> maxes;
        protected IList<double> averages;
        public bool PopulationEnabled { get; set; }
        public bool EvaluationEnabled { get; set; }
        public bool MutationEnabled { get; set; }
        public bool CrossOverEnabled { get; set; }
        public bool SelectionEnabled { get; set; }

        public Logger()
        {
            maxes = new List<double>();
            averages = new List<double>();
            //---start--- comment this section if u want logging to console. Uncoment => to file.
            filestream = new FileStream("out.txt", FileMode.Create);
            streamwriter = new StreamWriter(filestream);
            streamwriter.AutoFlush = true;
            Console.SetOut(streamwriter);
            consoleOutWasChanged = true;
            //---end----
        }

        public virtual void PrintPopulation<T>(Population<T> population, int generation)
        {
            if (PopulationEnabled)
            {
                if(consoleOutWasChanged && generation % 10 == 0) //this writes sth to console when writing to file was enabled
                {
                    Console.SetOut(oldOut);
                    Console.WriteLine("Generation: " + generation);
                    Console.SetOut(streamwriter);
                }
                Console.WriteLine("Generation: " + generation);
                Console.WriteLine("Population: ");

                IEnumerator<Chromosome> chromosomesEnumerator = population.Chromosomes.GetEnumerator();
                IEnumerator<double> evaluationEnumerator = population.ChromosomesEvaluation.GetEnumerator();
                int chromosomeIndex = 0;
                while (chromosomesEnumerator.MoveNext() && evaluationEnumerator.MoveNext())
                {
                    Console.Write(string.Format("{0, -4}", chromosomeIndex++ + "."));
                    var genes = chromosomesEnumerator.Current.Genes.ToList();
                    genes.ForEach(x => Console.Write(string.Format("{0, 2}", x) + " "));
                    //Console.SetCursorPosition(genes.Count * 2 + 10, Console.CursorTop);
                    Console.Write("\tEvaluation: " + Math.Round(evaluationEnumerator.Current, 3) + "\n");
                    //Console.WriteLine();
                }
            }
        }

        public virtual void PrintEvaluation(IEnumerable<double> chromosomesEvaluation)
        {
            maxes.Add(Math.Round(chromosomesEvaluation.Max(), 3));
            averages.Add(Math.Round(chromosomesEvaluation.Average(), 3));
            if (EvaluationEnabled)
            {
                Console.WriteLine("\nMax:".PadRight(10) + Math.Round(chromosomesEvaluation.Max(), 3));
                Console.WriteLine("Average:".PadRight(10) + Math.Round(chromosomesEvaluation.Average(), 3));
                Console.WriteLine();
            }
        }

        public virtual void PrintFooter()
        {
            Console.WriteLine(new string('-', 50));
        }

        public virtual void PrintMutation<T>(MutationFiredEventArgs e, IEncode<T> encodingFunction)
        {
            if (MutationEnabled)
            {
                Console.WriteLine("\nMutation: ");
                e.InitialChromosome.Genes.ToList().ForEach(x => Console.Write(string.Format("{0, 3}", x) + " "));
                Console.WriteLine(" => ");
                e.MutatedChromosome.Genes.ToList().ForEach(x => Console.Write(string.Format("{0, 3}", x) + " "));
                //Console.WriteLine("");
            }
        }

        public virtual void PrintCrossOver<T>(CrossOverFiredEventArgs e, IEncode<T> encodingFunction)
        {
            if (CrossOverEnabled)
            {
                Console.WriteLine("\nCrossOver:");
                e.InitialChromosome1.Genes.ToList().ForEach(x => Console.Write(x + " "));
                Console.Write(" => ");
                e.CrossOveredChromosome1.Genes.ToList().ForEach(x => Console.Write(x + " "));
                Console.WriteLine();
                e.InitialChromosome2.Genes.ToList().ForEach(x => Console.Write(x + " "));
                Console.Write(" => ");
                e.CrossOveredChromosome2.Genes.ToList().ForEach(x => Console.Write(x + " "));
                Console.WriteLine();
                Console.WriteLine("Index1: " + e.index1 + " Index2: " + e.index2);   
            }
        }

        public virtual void PrintSelection(SelectionFiredEventArgs e)
        {
            if (SelectionEnabled)
            {
                Console.WriteLine("Selection: index: " + e.SelectedIndex);
            }
        }

        public virtual void PrintResults<T>(GeneticSimulation<T> geneticSimulation)
        {
            //---start--- comment this section if u want logging to console. Uncoment => to file.
            Console.SetOut(oldOut);
            streamwriter.Close();
            filestream.Close();
            //---end----
            StatisticWindow statisticWindow = new StatisticWindow();
            statisticWindow.MaxYAxis = geneticSimulation.DesiredEvaluation;
            statisticWindow.AverageChartData = averages;
            statisticWindow.MaxChartData = maxes;
            statisticWindow.SetAxisInterval(geneticSimulation.Iterations);
            statisticWindow.ShowDialog();
        }
    }
}
