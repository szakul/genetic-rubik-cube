﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace GeneticRubikCube
{
    public partial class StatisticWindow : Form
    {
        DataPoint[] initialMaxDataset;
        DataPoint[] initialAvgDataset;


        public double MaxYAxis
        {
            get
            {
                return maxChart.ChartAreas[0].AxisY.Maximum;
            }
            set
            {
                maxChart.ChartAreas[0].AxisY.Maximum = value;
                averageChart.ChartAreas[0].AxisY.Maximum = value;
            }
        }
        public IList<double> MaxChartData
        {
            get
            {
                return maxChart.Series["Max"].Points.Select(x => x.YValues[0]).ToList();
            }
            set
            {
                for (int x = 0; x < value.Count; x++)
                {
                    maxChart.Series["Max"].Points.AddXY(x, value[x]);
                }
                initialMaxDataset = maxChart.Series["Max"].Points.ToArray();
            }
        }
        public IList<double> AverageChartData
        {
            get
            {
                return averageChart.Series["Average"].Points.Select(x => x.YValues[0]).ToList();
            }
            set
            {
                for (int x = 0; x < value.Count; x++)
                {
                    averageChart.Series["Average"].Points.AddXY(x, value[x]);
                }
                initialAvgDataset = averageChart.Series["Average"].Points.ToArray();
            }
        }

        public void AverageDataPoints(int pointCountToAverage)
        {
            averageChart.Series["Average"].Points.Clear();
            List<DataPoint> newPointComponents = new List<DataPoint>();
            for (int i = 0; i < initialAvgDataset.Length; i++)
            {
                if((i + pointCountToAverage) % pointCountToAverage == 0)
                {
                    newPointComponents.Add(initialAvgDataset[i]);
                    averageChart.Series["Average"].Points.AddXY(i, newPointComponents.Average(x => x.YValues[0]));
                    newPointComponents.Clear();
                }
                else
                {
                    newPointComponents.Add(initialAvgDataset[i]);
                }
            }

            maxChart.Series["Max"].Points.Clear();
            newPointComponents = new List<DataPoint>();
            for (int i = 0; i < initialMaxDataset.Length; i++)
            {
                if ((i + pointCountToAverage) % pointCountToAverage == 0)
                {
                    newPointComponents.Add(initialMaxDataset[i]);
                    maxChart.Series["Max"].Points.AddXY(i, newPointComponents.Average(x => x.YValues[0]));
                    newPointComponents.Clear();
                }
                else
                {
                    newPointComponents.Add(initialMaxDataset[i]);
                }
            }

            SetAxisInterval((int)initialMaxDataset.Length);
        }


        public void SetAxisInterval(int maxX)
        {
            maxChart.ChartAreas[0].AxisX.Minimum = 0;
            averageChart.ChartAreas[0].AxisX.Minimum = 0;

            maxChart.ChartAreas[0].AxisX.Maximum = maxX;
            averageChart.ChartAreas[0].AxisX.Maximum = maxX;

            maxChart.ChartAreas[0].AxisX.Interval = maxX / 100;
            averageChart.ChartAreas[0].AxisX.Interval = maxX / 100;
        }

        public StatisticWindow()
        {
            InitializeComponent();
            numericUpDown1.ValueChanged += NumericUpDown1_ValueChanged;
            splitContainer1.Height = this.Height - numericUpDown1.Height;
            splitContainer1.SplitterDistance = splitContainer1.Height / 2;
            maxChart.Height = splitContainer1.Height / 2;
            averageChart.Height = splitContainer1.Height / 2;
            numericUpDown1.Location = new Point(0, this.Height - numericUpDown1.Height);
            numericUpDown1.Width = this.Width;
        }

        private void NumericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            AverageDataPoints(Convert.ToInt32(numericUpDown1.Value));

            this.Refresh();
        }

        private void averageChart_Click(object sender, EventArgs e)
        {

        }
    }
}
