﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Selection
{
    public class RankSelection : ISelection
    {
        private ProportionalSelector proportionalSelector;
        public event EventHandler<SelectionFiredEventArgs> SelectionFired;
        public double SelectivePressure { get; } = 1.0;

        public RankSelection(Random randomGenerator)
        {
            proportionalSelector = new ProportionalSelector(randomGenerator);
            proportionalSelector.SelectionFired += (object sender, SelectionFiredEventArgs e) => SelectionFired(this, e);
        }

        public IEnumerable<Chromosome> GetRankedList(IEnumerable<Chromosome> chromosomes, IEnumerable<double> evaluations)
        {
            IEnumerator<double> evaluationEnumerator = evaluations.GetEnumerator();
            return chromosomes.OrderBy(x =>
            {
                evaluationEnumerator.MoveNext();
                return evaluationEnumerator.Current;
            });
        }

        public double ProbabilityFunction(double rank, double populationSize)
        {
            var a = ((2 - SelectivePressure) / populationSize);
            var b = 2 * rank * (SelectivePressure - 1);
            var c = populationSize * (populationSize - 1);
            return a + b / c;
        }

        public IList<Chromosome> Select<T>(Population<T> population)
        {
            var rankedChromosomes = GetRankedList(population.Chromosomes, population.ChromosomesEvaluation);
            decimal[] selectionProabilities = new decimal[population.Size];
            for (int i = population.Size - 1; i >= 0; i--)
            {
                selectionProabilities[i] = (decimal)ProbabilityFunction(i, population.Size);
            }

           var t =  selectionProabilities.Sum();
            t = t;

            return proportionalSelector.Select<T>(rankedChromosomes.ToArray(), selectionProabilities);
        }
    }
}
