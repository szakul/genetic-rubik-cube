﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Selection
{
    public class TournamentSelection : ISelection
    {
        private Random randomGenerator = new Random();
        private int tournamentGroupSize;

        public event EventHandler<SelectionFiredEventArgs> SelectionFired;
        public TournamentSelection(Random randomGenerator, int tournamentGroupSize)
        {
            this.randomGenerator = randomGenerator;
            this.tournamentGroupSize = tournamentGroupSize;
        }

        public IList<Chromosome> Select<T>(Population<T> popuation)
        {
            GroupSizeValidation(popuation.Size);
            IList<Chromosome> selectedChromosomes = new List<Chromosome>();
            //number of performed tournament is equal to  population size
            //numbers of chromosomes wich take part in single tournament is equal tournamentGroupSize (class member)
            //it is a posibility that some of chromosomes wont take part in any tournament
            for (int i = 0; i < popuation.Size; i++)
            {
                List<int> chromosomesInTournamentIndexes = GenerateTournamentGroup(popuation.Size);
                Dictionary<int, double> tournament = new Dictionary<int, double>(); //index by evaluation
                foreach(int chromosomeIndex in chromosomesInTournamentIndexes)
                {
                    tournament.Add(chromosomeIndex, popuation.ChromosomesEvaluation.ElementAt(chromosomeIndex));
                }
                double bestEvaluation = tournament.Values.Max();
                int winnerIndex = tournament.First(x => x.Value == bestEvaluation).Key;
                Chromosome selectedChromosome = new Chromosome(popuation.Chromosomes[winnerIndex]);
                selectedChromosomes.Add(selectedChromosome);
                SelectionFired.Invoke(this, new SelectionFiredEventArgs(winnerIndex));
            }
            return selectedChromosomes;
        }

        public List<int> GenerateTournamentGroup(int populatonSize)
        {
            List<int> chromosomesInGroupIndexes = new List<int>(populatonSize);
            for (int i = 0; i < tournamentGroupSize; i++)
            {
                int nextRandom = randomGenerator.Next(0, populatonSize);
                while (chromosomesInGroupIndexes.Contains(nextRandom))
                    nextRandom = randomGenerator.Next(0, populatonSize);
                chromosomesInGroupIndexes.Add(nextRandom);
            }
            return chromosomesInGroupIndexes;
        }

        private void GroupSizeValidation(int populationSize)
        {
            if (tournamentGroupSize < 1 || tournamentGroupSize > populationSize)
                throw new ArgumentException("Tournament group size must be greater than zero and lower or equal to population size");
        }
    }
}
