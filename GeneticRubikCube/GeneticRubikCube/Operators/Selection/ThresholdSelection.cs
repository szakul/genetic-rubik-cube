﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Selection
{
    public class ThresholdSelection : ISelection
    {
        private ProportionalSelector proportionalSelector;
        public event EventHandler<SelectionFiredEventArgs> SelectionFired;
        public decimal Gamma { get; private set; }

        public ThresholdSelection(Random randomGenerator, decimal Gamma)
        {
            this.Gamma = Gamma;
            proportionalSelector = new ProportionalSelector(randomGenerator);
            proportionalSelector.SelectionFired += (object sender, SelectionFiredEventArgs e) => SelectionFired(this, e);
        }

        public int[] GetChromosomesRanking(Chromosome[] chromosomes, double[] evaluations)
        {
            var evaluatedChromosomes = chromosomes.Zip(evaluations, (x, y) => new Tuple<Chromosome, double>(x, y));
            var sortedChromosomes = new Stack<Chromosome>(evaluatedChromosomes.OrderBy(x => x.Item2).Select(x => x.Item1));
            int[] ranking = new int[chromosomes.Length];
            for (int i = 0; i < chromosomes.Length; i++)
            {
                Chromosome actualTheBestChromosome = sortedChromosomes.Pop();
                int j = 0;
                while (chromosomes[j] != actualTheBestChromosome)
                {
                    j++;
                }
                ranking[j] = i;
            }
            return ranking;
        }

        public IList<Chromosome> Select<T>(Population<T> population)
        {
            int[] chromosomeRanking = GetChromosomesRanking(population.Chromosomes.ToArray(), population.ChromosomesEvaluation.ToArray());
            decimal[] selectionProabilities = new decimal[population.Size];
            decimal temp = Gamma * population.Size;
            if(temp < 1.0M)
            {
                throw new ArgumentException("Gamma * population.Size parameter can't be less than 1.0");
            }
            else
            {
                for (int i = 0; i < population.Size; i++)
                {
                    int rank = chromosomeRanking[i];
                    selectionProabilities[i] = rank >= 0 && rank < temp ? 1 / temp : 0;
                }
                return proportionalSelector.Select<T>(population.Chromosomes.ToArray(), selectionProabilities);
            }
        }
    }
}
