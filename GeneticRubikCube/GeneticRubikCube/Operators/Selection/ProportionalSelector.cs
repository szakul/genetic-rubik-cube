﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Selection
{
    internal class ProportionalSelector
    {
        private Random randomGenerator;
        public event EventHandler<SelectionFiredEventArgs> SelectionFired;

        public ProportionalSelector(Random randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }

        public IList<Chromosome> Select<T>(Chromosome[] chromosomes, decimal[] probabilities)
        {
            IList<Chromosome> selectedChromosomes = new List<Chromosome>();

            foreach (Chromosome chromosome in chromosomes)
            {
                int selectionIndex = 0;
                decimal rouletteWheel = (decimal)randomGenerator.NextDouble();
                decimal rouletteSum = probabilities[0];
                while (rouletteSum < rouletteWheel)
                {
                    rouletteSum += probabilities[++selectionIndex];
                }

                Chromosome selectedChromosome = new Chromosome(chromosomes[selectionIndex]);
                selectedChromosomes.Add(selectedChromosome);
                SelectionFired.Invoke(this, new SelectionFiredEventArgs(selectionIndex));
            }

            return selectedChromosomes;
        }
    }
}
