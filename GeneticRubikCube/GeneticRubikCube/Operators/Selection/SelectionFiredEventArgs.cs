﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Selection
{
    public class SelectionFiredEventArgs
    {
        public int SelectedIndex { get; }

        public SelectionFiredEventArgs(int selectedIndex)
        {
            SelectedIndex = selectedIndex;
        }
    }
}
