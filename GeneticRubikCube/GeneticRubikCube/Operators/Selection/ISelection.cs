﻿using GeneticRubikCube.Operators.Selection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators
{
    public interface ISelection
    {
        IList<Chromosome> Select<T>(Population<T> population);
        event EventHandler<SelectionFiredEventArgs> SelectionFired;
    }
}
