﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Selection
{
    public class RouletteSelection : ISelection
    {
        private Random randomGenerator;

        public event EventHandler<SelectionFiredEventArgs> SelectionFired;

        public RouletteSelection(Random randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }

        public decimal[] ComputeRouletteDivision(IEnumerable<double> chromosomesEvaluation)
        {
            double evaluationSum = chromosomesEvaluation.Sum();
            return chromosomesEvaluation.Select(x => ((decimal)x / (decimal)evaluationSum) * 100).ToArray();
        }

        public IList<Chromosome> Select<T>(Population<T> population)
        {
            //TODO: optimize
            IList<Chromosome> selectedChromosomes = new List<Chromosome>();
            decimal[] rouletteDivision = ComputeRouletteDivision(population.ChromosomesEvaluation).ToArray();

            foreach (Chromosome chromosome in population.Chromosomes)
            {
                int selectionIndex = 0;
                decimal rouletteWheel = randomGenerator.Next(0, 100);
                decimal rouletteSum = rouletteDivision[0];
                while (rouletteSum < rouletteWheel)
                {
                    rouletteSum += rouletteDivision[++selectionIndex];
                }

                Chromosome selectedChromosome = new Chromosome(population.Chromosomes[selectionIndex]);
                selectedChromosomes.Add(selectedChromosome);
                SelectionFired.Invoke(this, new SelectionFiredEventArgs(selectionIndex));
            }

            return selectedChromosomes;
        }
    }
}
