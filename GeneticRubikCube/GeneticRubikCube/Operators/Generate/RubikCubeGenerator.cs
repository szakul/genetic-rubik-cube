﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Generate
{
    public class RubikCubeGenerator : IGenerator<RubiksCube>
    {
        private int moves;

        public RubikCubeGenerator(int moves = 0)
        {
            this.moves = moves;
        }

        public RubiksCube Generate(IEnumerable<int> moves)
        {
            RubiksCube cube = new RubiksCube();
            foreach (var i in moves)
            {
                cube.Turn(i);
            }

            return cube;
        }

        public RubiksCube Generate()
        {
            RubiksCube cube = new RubiksCube();
            cube.Shuffle(moves);
            return cube;
        }
    }
}
