﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Generate
{
    public class DiscreteGenerator : IGenerator<int>
    {
        private Random randomGenerator;
        private int min;
        private int max;

        public DiscreteGenerator(int min, int max, Random randomGenerator)
        {
            this.min = min;
            this.max = max;
            this.randomGenerator = randomGenerator;
        }

        public int Generate()
        {
            return randomGenerator.Next(min, max);
        }
    }
}
