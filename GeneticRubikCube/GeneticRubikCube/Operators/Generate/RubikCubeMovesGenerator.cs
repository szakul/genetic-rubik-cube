﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Generate
{
    public class RubikCubeMovesGenerator : IGenerator<Chromosome>
    {
        private const int MIN = 0;
        private readonly int MAX;
        private Random randomGenerator;
        private int count;

        public RubikCubeMovesGenerator(int count, Random randomGenerator, bool allowNegligibleMoves = false)
        {
            this.count = count;
            this.randomGenerator = randomGenerator;
            //12 moves can be performed on rubic cube (from 0 to 11)
            //if negligible moves are allowed the 13'th move (with index 12) does nothing
            MAX = allowNegligibleMoves ? 12 : 11;
        }

        public Chromosome Generate()
        {
            List<int> moves = new List<int>();

            for (int i = 0; i < count; i++)
            {
                moves.Add(randomGenerator.Next(MIN, MAX + 1));
            }

            return new Chromosome(moves, MIN, MAX);
        }
    }
}
