﻿using GeneticRubikCube.Operators.Encode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Evaluation
{
    public class FunctionEvaluator : IEvaluation<int>
    {
        private Func<double, double> function;

        public FunctionEvaluator(Func<double, double> function)
        {
            this.function = function;
        }

        public double Evaluate(Chromosome chromosome, IEncode<int> decoder)
        {
            return function(decoder.Decode(chromosome));
        }
    }
}
