﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators
{
    public interface IEvaluation<T>
    {
        double Evaluate(Chromosome chromosome, IEncode<T> decoder);
    }
}
