﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Evaluation
{
    class RubiksCubeCompletePercentageEvaluator : IEvaluation<RubiksCube>
    {
        private Func<RubiksCube, double> function;

        public RubiksCubeCompletePercentageEvaluator()
        {
            this.function = CubeCompletePercentage;
        }
        public double Evaluate(Chromosome chromosome, IEncode<RubiksCube> decoder)
        {
            return function(decoder.Decode(chromosome));
        }

        private double CubeCompletePercentage(RubiksCube rubiksCube)
        {
            const double numberOfSquarsOnEachFace = 8;
            double squaresInProperPlace = 0;
            foreach(var face in rubiksCube.Faces)
            {
                squaresInProperPlace += EvaluateRubiksCubeFace(face);
            }
            return squaresInProperPlace / (rubiksCube.Faces.Length * numberOfSquarsOnEachFace);
        }

        private int EvaluateRubiksCubeFace(RubiksCubeFace rubiksCubeFace)
        {
            int faceEvaluation = -1;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (rubiksCubeFace.face[i, j] == rubiksCubeFace.GetColor())
                        faceEvaluation++; 
                }
            }
            return faceEvaluation;
        } 
    }
}
