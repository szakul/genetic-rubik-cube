﻿using GeneticRubikCube.Operators.CrossOver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators
{
    public interface ICrossOver
    {
        void CrossOver(Chromosome chromosome1, Chromosome chromosome2);
        event EventHandler<CrossOverFiredEventArgs> CrossOverFired;
    }
}
