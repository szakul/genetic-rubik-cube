﻿using GeneticRubikCube.Operators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.CrossOver
{
    class TwoPointCrossOver : ICrossOver
    {
        private Random randomGenerator;

        public event EventHandler<CrossOverFiredEventArgs> CrossOverFired;

        public TwoPointCrossOver(Random randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }

        public void CrossOver(Chromosome chromosome1, Chromosome chromosome2)
        {
            Chromosome initialChromosome1 = new Chromosome(chromosome1);
            Chromosome initialChromosome2 = new Chromosome(chromosome2);

            if(initialChromosome1.Genes.Count != 1 && initialChromosome2.Genes.Count != 1)
            {
                Chromosome smallerChromosome = initialChromosome1.Genes.Count < initialChromosome2.Genes.Count ? initialChromosome1 : initialChromosome2;

                Tuple<int, int> crossOverIndexes = GetTwoRandomNotEqualsNumbers(0, smallerChromosome.Genes.Count - 1);
                for (int i = crossOverIndexes.Item1; i < crossOverIndexes.Item2; i++)
                {
                    int temp = chromosome1[i];
                    chromosome1[i] = chromosome2[i];
                    chromosome2[i] = temp;
                }
                CrossOverFired.Invoke(this, new CrossOverFiredEventArgs(initialChromosome1, initialChromosome2, new Chromosome(chromosome1), new Chromosome(chromosome2), crossOverIndexes.Item1, crossOverIndexes.Item2));
            }        
        }
        private Tuple<int, int> GetTwoRandomNotEqualsNumbers(int from, int to)
        {
            int first = randomGenerator.Next(from, to + 1);
            int second = randomGenerator.Next(from, to + 1);
            while (first == second)
                first = randomGenerator.Next(from, to + 1);
            int lower = (first < second) ? first : second;
            int greater = (first < second) ? second : first;
            return new Tuple<int, int>(lower, greater);
        }
    }
}
