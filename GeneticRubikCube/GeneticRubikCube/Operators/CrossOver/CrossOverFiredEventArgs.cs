﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.CrossOver
{
    public class CrossOverFiredEventArgs
    {
        public Chromosome InitialChromosome1 { get; }
        public Chromosome InitialChromosome2 { get; }
        public Chromosome CrossOveredChromosome1 { get; }
        public Chromosome CrossOveredChromosome2 { get; }

        //TODO: Delete this
        public int index1 { get; }
        public int index2 { get; }

        public CrossOverFiredEventArgs(Chromosome initialChromosome1, Chromosome initialChromosome2, Chromosome crossOveredChromosome1, Chromosome crossOveredChromosome2, int index1, int index2)
        {
            this.index1 = index1;
            this.index2 = index2;
            this.InitialChromosome1 = initialChromosome1;
            this.InitialChromosome2 = initialChromosome2;
            this.CrossOveredChromosome1 = crossOveredChromosome1;
            this.CrossOveredChromosome2 = crossOveredChromosome2;
        }

    }
}
