﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.CrossOver
{
    public class SimpleCrossOver : ICrossOver
    {
        private Random randomGenerator;
        public event EventHandler<CrossOverFiredEventArgs> CrossOverFired;

        public SimpleCrossOver(Random randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }

        public void CrossOver(Chromosome chromosome1, Chromosome chromosome2)
        {
            Chromosome initialChromosome1 = new Chromosome(chromosome1);
            Chromosome initialChromosome2 = new Chromosome(chromosome2);

            Chromosome smallerChromosome = initialChromosome1.Genes.Count < initialChromosome2.Genes.Count ? initialChromosome1 : initialChromosome2;

            int crossOverIndex = randomGenerator.Next(1, smallerChromosome.Genes.Count - 1);
            for (int i = crossOverIndex; i < smallerChromosome.Genes.Count; i++)
            {
                int temp = chromosome1[i];
                chromosome1[i] = chromosome2[i];
                chromosome2[i] = temp;
            }

            CrossOverFired.Invoke(this, new CrossOverFiredEventArgs(initialChromosome1, initialChromosome2, new Chromosome(chromosome1), new Chromosome(chromosome2), crossOverIndex, 0));
        }
    }
}
