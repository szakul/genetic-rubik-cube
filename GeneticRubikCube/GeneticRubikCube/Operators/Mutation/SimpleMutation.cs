﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Mutation
{
    public class SimpleMutation : IMutation
    {
        private Random randomGenerator;
        public event EventHandler<MutationFiredEventArgs> MutationFired;

        public SimpleMutation(Random randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }

        public void Mutate(Chromosome chromosome, double geneMutationProbability)
        {
            Chromosome initialChromosome = new Chromosome(chromosome);
            bool wasMutated = false;

            for (int i = 0; i < chromosome.Genes.Count; i++)
            {
                int allel = chromosome[i];
                if (randomGenerator.NextDouble() <= geneMutationProbability)
                {
                    wasMutated = true;
                    chromosome[i] = randomGenerator.Next(chromosome.MinValue, chromosome.MaxValue + 1);
                }
            }
            if (wasMutated)
            {
                MutationFired.Invoke(this, new MutationFiredEventArgs(initialChromosome, chromosome));
            }
        }
    }
}
