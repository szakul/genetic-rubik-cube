﻿using GeneticRubikCube.Operators.Mutation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators
{
    public interface IMutation
    {
        void Mutate(Chromosome chromosome, double geneMutationProbability);
        event EventHandler<MutationFiredEventArgs> MutationFired;
    }
}
