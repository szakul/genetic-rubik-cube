﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Mutation
{
    public class ChromosomeLengthMutation : IMutation
    {
        private Random randomGenerator;
        public event EventHandler<MutationFiredEventArgs> MutationFired;

        public double LengthMutationProbability { get; }

        public int MaxChromosomeLength { get; }

        public ChromosomeLengthMutation(Random randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }

        public ChromosomeLengthMutation(Random randomGenerator, double lengthMutationProbability, int maxLength) : this(randomGenerator)
        {
            LengthMutationProbability = lengthMutationProbability;
            MaxChromosomeLength = maxLength;
        }

        public void Mutate(Chromosome chromosome, double geneMutationProbability)
        {
            Chromosome initialChromosome = new Chromosome(chromosome);
            bool wasMutated = false;

            if(chromosome.Genes.Count < MaxChromosomeLength && randomGenerator.NextDouble() <= LengthMutationProbability)
            {
                wasMutated = true;
                chromosome.Genes.Add(randomGenerator.Next(chromosome.MinValue, chromosome.MaxValue + 1));
            }
            else
            {
                for (int i = 0; i < chromosome.Genes.Count; i++)
                {
                    int allel = chromosome[i];
                    if (randomGenerator.NextDouble() <= geneMutationProbability)
                    {
                        wasMutated = true;
                        chromosome[i] = randomGenerator.Next(chromosome.MinValue, chromosome.MaxValue + 1);
                    }
                }
            }
            if (wasMutated)
            {
                MutationFired.Invoke(this, new MutationFiredEventArgs(initialChromosome, chromosome));
            }
        }
    }
}
