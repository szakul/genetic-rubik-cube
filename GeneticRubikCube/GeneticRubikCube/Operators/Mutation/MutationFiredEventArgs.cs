﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Mutation
{
    public class MutationFiredEventArgs
    {
        public Chromosome InitialChromosome { get; }
        public Chromosome MutatedChromosome { get; }

        public MutationFiredEventArgs(Chromosome initialChromosome, Chromosome mutatedChromosome)
        {
            InitialChromosome = initialChromosome;
            MutatedChromosome = mutatedChromosome;
        }


    }
}
