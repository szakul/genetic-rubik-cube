﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Encode
{
    public class BinaryEncoder : IEncode<int>
    {
        private int length;

        public BinaryEncoder(int length)
        {
            this.length = length;
        }

        public int Decode(Chromosome chromosome)
        {
            double result = 0;
            for (int i = 0; i < chromosome.Genes.Count; i++)
            {
                result += (chromosome[i] % 2 == 0) ? 0 : Math.Pow(2, i);
            }
            return (int)result;
        }

        public Chromosome Encode(int obj)
        {
            IList<int> genes = new List<int>();
            int tempObj = obj;
            for (int i = 0; i < length; i++)
            {
                genes.Add((obj % 2 == 0) ? 0 : 1);
                obj = obj >> 1;
            }
            return new Chromosome(genes, 0, 1);
        }
    }
}
