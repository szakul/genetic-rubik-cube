﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators.Encode
{
    public class RubiksCubeEncoder : IEncode<RubiksCube>
    {
        public RubiksCube InitialState { get; }
        public RubiksCubeEncoder(RubiksCube initialState)
        {
            this.InitialState = initialState;
        }
        public RubiksCube Decode(Chromosome chromosome)
        {
            RubiksCube decoded = InitialState.DeepCopy();
            chromosome.Genes.ToList().ForEach(move => decoded.Turn(move));
            return decoded;
        }

        public Chromosome Encode(RubiksCube obj)
        {
            throw new NotImplementedException();
        }
    }
}
