﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Operators
{
    public interface IEncode<T>
    {
        Chromosome Encode(T obj);
        T Decode(Chromosome chromosome);
    }
}
