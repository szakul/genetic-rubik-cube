﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneticRubikCube.Operators;
using GeneticRubikCube.Operators.Generate;
using GeneticRubikCube.Operators.Encode;
using GeneticRubikCube.Operators.Evaluation;
using GeneticRubikCube.Operators.Selection;
using GeneticRubikCube.Operators.CrossOver;
using GeneticRubikCube.Operators.Mutation;
using GeneticRubikCube.Examples;
using GeneticRubikCube.GeneticSimulation;

namespace GeneticRubikCube
{

    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            //Example<int> example = new FunctionExample();
            Example<RubiksCube> example = new RubikCubeExample();
            IGeneticSimulation geneticSimulation = example.GetSimulation();
            while (true)
            {
                geneticSimulation.Run();
                Console.ReadKey();
            }
        }
    }
}
