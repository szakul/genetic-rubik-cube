﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneticRubikCube.Operators;
using GeneticRubikCube.Operators.Generate;

namespace GeneticRubikCube
{
    public class InitialStatePopulation<T> : Population<T>
    {
        /*
         * Initial cube for which sequence of moves 
         * in chromosomes are refered
         */ 
        protected T initialState;

        public InitialStatePopulation(int size, double crossOverProbability, double mutationProbability, IList<Chromosome> chromosomes, IEncode<T> encodingFunction, T initialState, Random randomGenerator)
            : base(size, crossOverProbability, mutationProbability, chromosomes, encodingFunction, randomGenerator)
        {
            this.initialState = initialState;
        }
    }

}
