﻿using GeneticRubikCube.Operators;
using GeneticRubikCube.Operators.Selection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube
{
    public class GeneticSimulationBuilder<T>
    {
        protected Population<T> population;
        protected Func<IEnumerable<double>, double> populationEvaluation;
        protected IEvaluation<T> evaluationFunction;
        protected ISelection selectionFunction;
        protected ICrossOver crossOverFunction;
        protected IMutation mutationFunction;
        protected double desiredEvaluation;
        protected int iterations;

        public GeneticSimulationBuilder<T> Population(Population<T> population)
        {
            this.population = population;
            return this;
        }

        public GeneticSimulationBuilder<T> PopulationEvaluation(Func<IEnumerable<double>, double> populationEvaluation)
        {
            this.populationEvaluation = populationEvaluation;
            return this;
        }

        public GeneticSimulationBuilder<T> EvaluationFunction(IEvaluation<T> evaluationFunction)
        {
            this.evaluationFunction = evaluationFunction;
            return this;
        }

        public GeneticSimulationBuilder<T> SelectionFunction(ISelection selectionFunction)
        {
            this.selectionFunction = selectionFunction;
            return this;
        }
        public GeneticSimulationBuilder<T> CrossOverFunction(ICrossOver crossOverFunction)
        {
            this.crossOverFunction = crossOverFunction;
            return this;
        }

        public GeneticSimulationBuilder<T> MutationFunction(IMutation mutationFunction)
        {
            this.mutationFunction = mutationFunction;
            return this;
        }

        public GeneticSimulationBuilder<T> DesiredEvaluation(double desiredEvaluation)
        {
            this.desiredEvaluation = desiredEvaluation;
            return this;
        }
        public GeneticSimulationBuilder<T> Iterations(int iterations)
        {
            this.iterations = iterations;
            return this;
        }

        public GeneticSimulation<T> Build()
        {
            return new GeneticSimulation<T>(population, populationEvaluation, evaluationFunction, selectionFunction, crossOverFunction, mutationFunction, desiredEvaluation, iterations);
        }
    }
}
