﻿using GeneticRubikCube.GeneticSimulation;
using GeneticRubikCube.Operators;
using GeneticRubikCube.Operators.CrossOver;
using GeneticRubikCube.Operators.Mutation;
using GeneticRubikCube.Operators.Selection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube
{
    public class GeneticSimulation<T> : IGeneticSimulation
    {
        public Population<T> Population { get; private set; }
        public Func<IEnumerable<double>, double> PopulationEvaluator { get; }
        public IEvaluation<T> EvaluationFunction { get; }
        public ISelection SelectionFunction { get; }
        public ICrossOver CrossOverFunction { get; }
        public IMutation MutationFunction { get; }
        public double DesiredEvaluation { get; }
        public int Iterations { get; }
        protected Logger logger;

        public GeneticSimulation(Population<T> population, Func<IEnumerable<double>, double> populationEvaluation, IEvaluation<T> evaluationFunction, ISelection selectionFunction, ICrossOver crossOverFunction, IMutation mutationFunction, double desiredEvaluation, int iterations)
        {
            this.Population = population;
            this.Iterations = iterations;
            this.EvaluationFunction = evaluationFunction;
            this.SelectionFunction = selectionFunction;
            this.CrossOverFunction = crossOverFunction;
            this.MutationFunction = mutationFunction;
            this.DesiredEvaluation = desiredEvaluation;
            this.Iterations = iterations;
            this.PopulationEvaluator = populationEvaluation;
            InitializeLogger();
            SubscribeEvents();
        }

        public void InitializeLogger()
        {
            logger = new Logger();
            logger.PopulationEnabled = true;
            logger.EvaluationEnabled = true;
            logger.SelectionEnabled = true;
            logger.MutationEnabled = true;
            logger.CrossOverEnabled = true;
        }

        public void SubscribeEvents()
        {
            MutationFunction.MutationFired += ((object sender, MutationFiredEventArgs e) => logger.PrintMutation(e, Population.EncodingFunction));
            CrossOverFunction.CrossOverFired += ((object sender, CrossOverFiredEventArgs e) => logger.PrintCrossOver(e, Population.EncodingFunction));
            SelectionFunction.SelectionFired += ((object sender, SelectionFiredEventArgs e) => logger.PrintSelection(e));
        }


        public virtual void Run()
        {
            double[] populationEvaluation = new double[Iterations];
            for (int generation = 0; generation < Iterations; generation++)
            {
                populationEvaluation[generation] = Population.Evaluate(EvaluationFunction, PopulationEvaluator);
                logger.PrintPopulation(Population, generation);
                logger.PrintEvaluation(Population.ChromosomesEvaluation);
                if (populationEvaluation[generation] >= DesiredEvaluation)
                    break;
                Population = Population.Selection(SelectionFunction);
                Population.CrossOver(CrossOverFunction);
                Population.Mutation(MutationFunction);
                logger.PrintFooter();
            }
            logger.PrintResults(this);
        }
    }
}
