﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeneticRubikCube.Operators;
using System.Diagnostics;

namespace GeneticRubikCube
{
    [System.Diagnostics.DebuggerDisplay("{ToString()}")]
    public class Chromosome
    {
        public IList<int> Genes { get; }
        public int MaxValue { get; }
        public int MinValue { get; }

        public int this[int i]
        {
            get
            {
                return Genes[i];
            }
            set
            {
                Genes[i] = value;
            }
        }

        public Chromosome(IList<int> genes, int minValue, int maxValue)
        {
            this.Genes = genes;
            this.MinValue = minValue;
            this.MaxValue = maxValue;
        }

        public Chromosome(Chromosome chromosome)
        {
            this.Genes = new List<int>(chromosome.Genes);
            this.MinValue = chromosome.MinValue;
            this.MaxValue = chromosome.MaxValue;
        }

        public override string ToString()
        {
            string y = "GENES : [";
            foreach (int gene in Genes)
            {
                y = y + gene + ",";
            }
            y.Remove(y.Length - 1);
            y = y + "]";
            return y;
        }
    }
}
