﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GeneticRubikCube.Operators.Selection;

namespace GeneticRubikCube.Tests
{
    class TournamentSelectionTest
    {
        [Test]
        public void GenerateTournamentGroupShouldGenerateListOfIndexesFromZeroToPopulationSize()
        {
            //given
            int tournamentGroupSize = 2;
            int populationSize = 4;
            var x = Enumerable.Range(0, populationSize).Select(i => (double)i).ToList();
            RandomGeneratorMock rgm = new RandomGeneratorMock(x);
            TournamentSelection ts = new TournamentSelection(rgm, tournamentGroupSize);
            List<int> expectedFirstGroup = new List<int>() { 0, 1 };
            List<int> expectedSecondGroup = new List<int>() { 2, 3 };

            //when
            List<int> actualFirstGroup = ts.GenerateTournamentGroup(populationSize);
            List<int> actualScondGroup = ts.GenerateTournamentGroup(populationSize);

            //then
            CollectionAssert.AreEqual(expectedFirstGroup, actualFirstGroup);
            CollectionAssert.AreEqual(expectedSecondGroup, expectedSecondGroup);
        }
    }
}
