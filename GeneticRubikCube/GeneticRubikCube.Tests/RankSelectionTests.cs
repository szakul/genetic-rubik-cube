﻿using GeneticRubikCube.Operators.Selection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Tests
{
    [TestFixture]
    public class RankSelectionTests
    {
        [Test]
        public void GetRankedList_DifferentEvaluations_ReturnedSortedChromosomes()
        {
            RankSelection selection = new RankSelection(new Random());
            IEnumerable<Chromosome> chromosomes = new List<Chromosome>()
            {
                new Chromosome(new List<int>() { 1,2,3 }, 1, 3),
                new Chromosome(new List<int>() { 2,1,3 }, 1, 3),
                new Chromosome(new List<int>() { 3,2,3 }, 1, 3)
            };
            IEnumerable<double> evaluation = new List<double>()
            {
                3.0,
                2.0,
                1.0
            };
            var result = selection.GetRankedList(chromosomes, evaluation);
            Assert.IsTrue(result.SequenceEqual(chromosomes.Reverse()));
        }
    }
}
