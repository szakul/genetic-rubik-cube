﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Tests
{
    public class RandomGeneratorMock : Random
    {
        protected ICollection<double> numberList;
        protected IEnumerator<double> numberPtr;
        public RandomGeneratorMock(ICollection<double> numberList)
        {
            this.numberList = numberList;
            numberPtr = numberList.GetEnumerator();
        }

        public override int Next()
        {
            return (int)GetNextNumber();
        }

        public override int Next(int minValue, int maxValue)
        {
            for (int i = 0; i < numberList.Count; i++)
            {
                int nextValue = (int)GetNextNumber();
                if (nextValue >= minValue && nextValue < maxValue)
                    return nextValue;
            }
            throw new InvalidOperationException("There is no value in numberList wich is in required range");
        }

        public override double NextDouble()
        {
            return GetNextNumber();
        }
         
        private double GetNextNumber()
        {
            if (!numberPtr.MoveNext())
            {
                numberPtr.Reset();
                numberPtr.MoveNext();
            }
            return numberPtr.Current;
        }

    }
}
