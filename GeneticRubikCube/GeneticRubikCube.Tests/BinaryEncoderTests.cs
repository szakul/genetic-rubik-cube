﻿using GeneticRubikCube.Operators.Encode;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Tests
{
    [TestFixture]
    class BinaryEncoderTests
    {
        [Test]
        public void Decode()
        {
            BinaryEncoder a = new BinaryEncoder(5);
            Chromosome chromosome = new Chromosome(new List<int>() { 0,0,0,0,0 }, 0, 30);
            Chromosome chromosome1 = new Chromosome(new List<int>() { 1, 0, 0, 0, 0 }, 0, 30 );
            Chromosome chromosome2 = new Chromosome(new List<int>() { 0, 1, 0, 0, 0 }, 0, 30);
            Chromosome chromosome3 = new Chromosome(new List<int>() { 1, 0, 0, 1, 0 }, 0, 30);
            Chromosome chromosome4 = new Chromosome(new List<int>() { 0, 0, 0, 0, 1 }, 0, 30);

            Assert.AreEqual(0, a.Decode(chromosome));
            Assert.AreEqual(1, a.Decode(chromosome1));
            Assert.AreEqual(2, a.Decode(chromosome2));
            Assert.AreEqual(9, a.Decode(chromosome3));
            Assert.AreEqual(16, a.Decode(chromosome4));
        }
    }
}
