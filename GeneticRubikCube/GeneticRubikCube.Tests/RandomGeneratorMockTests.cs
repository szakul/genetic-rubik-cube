﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticRubikCube.Tests
{
    [TestFixture]
    class RandomGeneratorMockTests
    {
        [Test]
        public void Next_ProperNumberListInConstructor_AreNumbersReturnedInCorrectOrder()
        {
            ICollection<double> numberList = new double[] { 1, 4, 7, 8 };
            ICollection<double> expectedResult = numberList;
            RandomGeneratorMock randomMock = new RandomGeneratorMock(numberList);

            int[] result = new int[numberList.Count];
            for (int i = 0; i < numberList.Count; i++)
            {
                result[i] = randomMock.Next();
            }

            Assert.AreEqual(numberList, result);
        }

        [Test]
        public void Next_ProperNumberListInConstructor_AreNumbersReturnedRepeatley()
        {
            ICollection<double> numberList = new double[] { 1, 4, 7, 8 };
            ICollection<double> expectedResult = new double[] { 1, 4, 7, 8, 1, 4, 7, 8 };
            RandomGeneratorMock randomMock = new RandomGeneratorMock(numberList);

            int[] result = new int[numberList.Count * 2];
            for (int i = 0; i < numberList.Count * 2; i++)
            {
                result[i] = randomMock.Next();
            }

            Assert.AreEqual(expectedResult, result);
        }

    }

}
