﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogViewer
{
    class LogFileReader
    {
        static List<double> maxes = new List<double>();
        static List<double> averages = new List<double>();
        public string[] Read(string logFilePath)
        {
            maxes.Clear();
            averages.Clear();
            string separator = new string('-', 50);
            string[] separatorArray = new string[] { separator };
            string text = File.ReadAllText(logFilePath);
            string[] generations = text.Split(separatorArray, StringSplitOptions.RemoveEmptyEntries);
            foreach (string generation in generations)
            {
                int toSkip = generation.IndexOf("Max:") + "Max:".Length;
                string unParsed = new string(generation.Skip(toSkip).TakeWhile(x => !x.Equals('\n')).ToArray());
                double max = -1;
                double.TryParse(unParsed, NumberStyles.Any, CultureInfo.CurrentCulture, out max);
                maxes.Add(max);

                toSkip = generation.IndexOf("Average:") + "Average:".Length;
                unParsed = new string(generation.Skip(toSkip).TakeWhile(x => !x.Equals('\n')).ToArray());
                double average = -1;
                double.TryParse(unParsed, NumberStyles.Any, CultureInfo.CurrentCulture, out average);
                averages.Add(average);
            }
            return generations;
        }
        public double FirstMax
        {
            get { return maxes[0]; }
        }
        public double FirstAverage
        {
            get { return averages[0]; }
        }
        public double MaxMax
        {
            get { return maxes.Max(); }
        }
        public double MaxAverage
        {
            get { return averages.Max(); }
        }
        public int IndexOfMaxMax
        {
            get { return maxes.IndexOf(MaxMax); }
        }
        public int IndexOfMaxAverage
        {
            get { return averages.IndexOf(MaxAverage); }
        }
    }
}
