﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LogViewer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string[] allGenerations;
        int generationsCount;
        int currentGenerationIndex;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void chooseLogFile_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text|*.txt|All|*.*";

            // Display OpenFileDialog by calling ShowDialog method 
            bool? result = dlg.ShowDialog();

            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                LogFileReader lfr = new LogFileReader();
                // Open document 
                string filename = dlg.FileName;
                // Read document
                allGenerations = lfr.Read(filename);
                currentGenerationIndex = 0;
                generationsCount = allGenerations.Length - 1;
                // Set windows controls
                generation.Text = currentGenerationIndex.ToString(new CultureInfo("en-US"));
                log.Text = allGenerations[currentGenerationIndex];
                countValue.Content = generationsCount;
                firstAverageValue.Text = lfr.FirstAverage.ToString(new CultureInfo("en-US"));
                firstMaxValue.Text = lfr.FirstMax.ToString(new CultureInfo("en-US"));
                maxAverageValue.Text = lfr.MaxAverage.ToString(new CultureInfo("en-US")) + " [" + lfr.IndexOfMaxAverage + "]";
                maxMaxValue.Text = lfr.MaxMax.ToString(new CultureInfo("en-US")) + " [" + lfr.IndexOfMaxMax + "]";
                differenceAverageValue.Text = (lfr.MaxAverage - lfr.FirstAverage).ToString(new CultureInfo("en-US"));
                differenceMaxValue.Text = (lfr.MaxMax - lfr.FirstMax).ToString(new CultureInfo("en-US"));
            }
        }

        private void next_Click(object sender, RoutedEventArgs e)
        {
            currentGenerationIndex++;
            CurrentGenerationIndexChanged();
        }

        private void previous_Click(object sender, RoutedEventArgs e)
        {
            currentGenerationIndex--;
            CurrentGenerationIndexChanged();
        }

        private void generation_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (allGenerations != null)
            {
                int newIndex = 0;
                currentGenerationIndex = int.TryParse(generation.Text, out newIndex) ? newIndex : currentGenerationIndex;
                ValidateCurrentGenerationIndex();
                log.Text = allGenerations[currentGenerationIndex];
            }
        }
        private void CurrentGenerationIndexChanged()
        {
            ValidateCurrentGenerationIndex();
            generation.Text = currentGenerationIndex.ToString();
            log.Text = allGenerations[currentGenerationIndex];
        }
        private void ValidateCurrentGenerationIndex()
        {
            if (currentGenerationIndex > generationsCount)
                currentGenerationIndex = generationsCount;
            else if (currentGenerationIndex < 0)
                currentGenerationIndex = 0;
        }

    }
}
